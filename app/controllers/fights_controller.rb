class FightsController < ApplicationController
    before_action :fight, except: [:update]
    def index
        respond_to do |format| 
        format.html
        format.json {}
        end
    end
    
    def test
    end
    
    def update
        @sum = -1
        @fight = Fight.find_by(team_one: params[:id], state: true)
        @fight2 = Fight.find_by(team_two: params[:id], state: true)
        if @fight != nil
            @fight.count_one += 1
            @fight.save
            @sum = @fight.count_one + @fight.count_two
            render json: {"team1": @fight.count_one,"team2": @fight.count_two}
        elsif @fight2 != nil
            @fight2.count_two += 1
            @fight2.save
            @sum = @fight2.count_one + @fight2.count_two
            render json: {"team1": @fight2.count_one, "team2": @fight2.count_two}
        end
        
    end
    
    def data
    end
    
    def state
        tmp = @fight_id
        if @fight_id.state
            @fight_id.state = false
        else
            @fight_id.state = true
        end
        @fight_id.save
    end
    
    def reset
        @fight_id.state = false
    end
    
    private
    
    def fight
        @fight_id = Fight.find(params[:id])
        @team1 = Team.find(@fight_id.team_one)
        @team2 = Team.find(@fight_id.team_two)
        @sum = @fight_id.count_one + @fight_id.count_two
    end
    
end
