Rails.application.routes.draw do
  get 'fight/:id' => 'fights#index', as: :fight
  get 'fight/:id/show' => 'fights#test', as: :fight_test
  get 'fight' => 'fights#update'
  get 'fight/:id/data' => 'fights#data'
  get 'fight/:id/state' => 'fights#state'
  get 'fight/:id/reset' => 'fights#reset'
end
