class CreateFights < ActiveRecord::Migration[5.1]
  def change
    create_table :fights do |t|
      t.integer :team_one
      t.integer :team_two
      t.integer :count_one, default: 0
      t.integer :count_two, default: 0
      t.boolean :state , default: false
      t.timestamps
    end
  end
end
